import {UniversalController} from "./ark/Universal.controller";
import {UserEntity} from "../entity/UserEntity";

export class UserController extends UniversalController {
    constructor() {
        super(
            UserEntity,
            'users');
    }
}