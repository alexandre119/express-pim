import {UniversalController} from "./Universal.controller";
import {EspeceEntity} from "../../entity/ark/Espece.entity";

export class EspeceController extends UniversalController {
    constructor() {
        super(EspeceEntity,'espece');
    }
}