import {UniversalController} from "./Universal.controller";
import {MouvementLotEntity} from "../../entity/ark/MouvementLot.entity";

export class MouvementLotController extends UniversalController {
    constructor() {
        super(
            MouvementLotEntity,
            'mouvementlot'
        );
    }
}