import {UniversalController} from "./Universal.controller";
import {SalarieEntity} from "../../entity/ark/Salarie.entity";

export class SalarieController extends UniversalController {
    constructor() {
        super(
            SalarieEntity,
            'salarie'
        );
    }
}