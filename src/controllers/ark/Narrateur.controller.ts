import {UniversalController} from "./Universal.controller";
import {NarrateurEntity} from "../../entity/ark/Narrateur.entity";

export class NarrateurController extends UniversalController {
    constructor() {
        super(
            NarrateurEntity,
            'narrateur'
        );
    }
}