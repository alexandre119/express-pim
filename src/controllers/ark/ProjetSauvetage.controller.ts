import {UniversalController} from "./Universal.controller";
import {ProjetSauvetageEntity} from "../../entity/ark/ProjetSauvetage.entity";

export class ProjetSauvetageController extends UniversalController {
    constructor() {
        super(
            ProjetSauvetageEntity,
            'projetsauvetage'
        );
    }
}