import {UniversalController} from "./Universal.controller";
import {OrdreEntity} from "../../entity/ark/Ordre.entity";

export class OrdreController extends UniversalController {
    constructor() {
        super(
            OrdreEntity,
            'ordre');
    }
}