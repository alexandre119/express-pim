import {UniversalController} from "./Universal.controller";
import {ClasseEntity} from "../../entity/ark/Classe.entity";

export class ClasseController extends UniversalController {
    constructor() {
        super(ClasseEntity,'classe');
    }
}