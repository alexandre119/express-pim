import {UniversalController} from "./Universal.controller";
import {PaysEntity} from "../../entity/ark/Pays.entity";

export class PaysController extends UniversalController {
    constructor() {
        super(
            PaysEntity,
            'pays'
        );
    }
}