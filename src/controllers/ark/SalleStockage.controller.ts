import {UniversalController} from "./Universal.controller";
import {SalleStockageEntity} from "../../entity/ark/SalleStockage.entity";

export class SalleStockageController extends UniversalController {
    constructor() {
        super(
            SalleStockageEntity,
            'sallestockage'
        );
    }
}