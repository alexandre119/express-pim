import {getRepository} from "typeorm";
import {RegneEntity} from "../../entity/ark/Regne.entity";
import {NextFunction, Request, Response} from "express";
import {UniversalController} from "./Universal.controller";

/*
export class RegneController {
    
    private regneRepository = getRepository(RegneEntity);
    
    async all(req: Request, res: Response, next: NextFunction) {
        return this.regneRepository.find();
    }
    
    async one(req: Request, res: Response, next: NextFunction) {
        let result = this.regneRepository.findOne(req.params.id);
        if ( typeof result !== 'undefined' ) {
            return result
        } else {
            let err = 'Objet non trouvé'
            console.log(err);
            return err
        }
    }
    
    async save(req: Request, res: Response, next: NextFunction) {
        return this.regneRepository.save(req.body);
    }
    
    async update(req: Request, res: Response, next: NextFunction) {
        return this.regneRepository.update(req.params.id, req.body)
    }
    
    async remove(req: Request, res: Response, next: NextFunction) {
        let toRemove = await this.regneRepository.findOne(req.params.id);
        if ( typeof toRemove !== 'undefined' ) {
            await this.regneRepository.remove(toRemove);
        } else {
            let err = 'Objet à supprimer non trouvé'
            console.log(err);
            return err
        }
    }
}
 */

export class RegneController extends UniversalController {
    constructor() {
        super(
            RegneEntity,
            'regne');
    }
}