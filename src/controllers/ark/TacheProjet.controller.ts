import {UniversalController} from "./Universal.controller";
import {TacheProjetEntity} from "../../entity/ark/TacheProjet.entity";

export class TacheProjetController extends UniversalController {
    constructor() {
        super(
            TacheProjetEntity,
            'tacheprojet');
    }
}