import {UniversalController} from "./Universal.controller";
import {GenreEntity} from "../../entity/ark/Genre.entity";

export class GenreController extends UniversalController {
    constructor() {
        super(
            GenreEntity,
            'genre');
    }
}