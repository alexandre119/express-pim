import {UniversalController} from "./Universal.controller";
import {AlerteEntity} from "../../entity/ark/Alerte.entity";

export class AlerteController extends UniversalController {
    constructor() {
        super(
            AlerteEntity,
            'alerte'
        );
    }
}