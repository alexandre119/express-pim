import {UniversalController} from "./Universal.controller";
import {SentinelleEntity} from "../../entity/ark/Sentinelle.entity";

export class SentinelleController extends UniversalController {
    constructor() {
        super(
            SentinelleEntity,
            'sentinelle'
        );
    }
}