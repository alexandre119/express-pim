import {UniversalController} from "./Universal.controller";
import {LivreSauvetageEntity} from "../../entity/ark/LivreSauvetage.entity";

export class LivreSauvetageController extends UniversalController {
    constructor() {
        super(
            LivreSauvetageEntity,
            'livresauvetage'
        );
    }
}