import {UniversalController} from "./Universal.controller";
import {FamilleEntity} from "../../entity/ark/Famille.entity";

export class FamilleController extends UniversalController {
    constructor() {
        super(FamilleEntity,'famille');
    }
}