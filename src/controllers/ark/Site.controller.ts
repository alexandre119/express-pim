import {UniversalController} from "./Universal.controller";
import {SiteEntity} from "../../entity/ark/Site.entity";

export class SiteController extends UniversalController {
    constructor() {
        super(
            SiteEntity,
            'site'
        );
    }
}