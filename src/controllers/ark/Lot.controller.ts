import {UniversalController} from "./Universal.controller";
import {LotEntity} from "../../entity/ark/Lot.entity";

export class LotController extends UniversalController {
    constructor() {
        super(
            LotEntity,
            'lot'
        );
    }
}