import {UniversalController} from "./Universal.controller";
import {CommuneEntity} from "../../entity/ark/Commune.entity";

export class CommuneController extends UniversalController {
    constructor() {
        super(
            CommuneEntity,
            'commune'
        );
    }
}