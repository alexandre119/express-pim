import {UniversalController} from "./Universal.controller";
import {EmbranchementEntity} from "../../entity/ark/Embranchement.entity";

export class EmbranchementController extends UniversalController {
    constructor() {
        super(
            EmbranchementEntity,
            'embranchement'
        );
    }
}