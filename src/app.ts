import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import {Request, Response} from "express";

import router, {CreateRoutes} from './routes/routes';
import {createConnection} from "typeorm";

const port: number = 9999;
const host: string = 'localhost'; // Don't put protocol

// Connect to database with TypeORM
createConnection().then(x => {
    console.log(x)
    // Create express router
    const app: express.Application = express();
    app.use(cors({
        exposedHeaders: ['Content-Disposition', "X-Header-Filename"],
        allowedHeaders: '*',
        origin: "*",
        methods: "*"
    }));
    app.use(bodyParser.json());
    app.use('/', router);
    app.listen(port, host);
    console.log("Server started at http://%s:%d", host, port);
    
    // Creation des routes principales et de la liste des tables
    const Routes = CreateRoutes();
    
    // Generation des routes pour chaque tables
    Routes.forEach(route => {
        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);
                console.log(result)
            } else if (result !== null && result !== undefined) {
                res.json(result);
                console.log(result)
            }
        });
    });
}).catch(error => console.log(error));