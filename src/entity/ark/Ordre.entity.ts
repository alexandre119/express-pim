import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {ClasseEntity} from "./Classe.entity";
import {FamilleEntity} from "./Famille.entity";

@Entity('Ordre')
export class OrdreEntity extends IdAndNameEntity{
    @ManyToOne(() => ClasseEntity, classe => classe.ordres, { nullable: false })
    classe?: ClasseEntity;
    
    @OneToMany(() => FamilleEntity, famille => famille.ordre)
    familles?: FamilleEntity[]
}