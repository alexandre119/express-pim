import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity('IdName')
export class IdAndNameEntity {
    @PrimaryGeneratedColumn()
    id?: number;
    
    @Column("varchar", { nullable: false })
    name?: string;
}