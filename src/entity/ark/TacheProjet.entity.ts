import {Column, Entity, OneToMany} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {MouvementLotEntity} from "./MouvementLot.entity";

@Entity('TacheProjet')
export class TacheProjetEntity extends IdAndNameEntity {
    @Column()
    type?: string
    
    @Column()
    description?: string
    
    @OneToMany(() => MouvementLotEntity, mouvements => mouvements.tache)
    mouvements?: MouvementLotEntity
}