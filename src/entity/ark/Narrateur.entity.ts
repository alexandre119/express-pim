import {Entity, OneToMany} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {ProjetSauvetageEntity} from "./ProjetSauvetage.entity";

@Entity('Narrateur')
export class NarrateurEntity extends IdAndNameEntity {
    @OneToMany(() => ProjetSauvetageEntity, projetsSauvetage => projetsSauvetage.narrateur)
    projetsSauvetage?: ProjetSauvetageEntity[]
}
