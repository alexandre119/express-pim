import {Column, Entity, ManyToOne, OneToMany, OneToOne} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {NarrateurEntity} from "./Narrateur.entity";
import {AlerteEntity} from "./Alerte.entity";
import {LivreSauvetageEntity} from "./LivreSauvetage.entity";

@Entity('ProjetSauvetage')
export class ProjetSauvetageEntity extends IdAndNameEntity {
    @Column()
    description?: string
    
    @ManyToOne(() => NarrateurEntity, narrateur => narrateur.projetsSauvetage)
    narrateur?: NarrateurEntity
    
    @ManyToOne(() => AlerteEntity, alerte => alerte.projetsSauvetage)
    alerte?: AlerteEntity
    
    @OneToMany(() => LivreSauvetageEntity, livreSauvetage => livreSauvetage.projetSauvetage)
    livreSauvetage?: LivreSauvetageEntity[]
}