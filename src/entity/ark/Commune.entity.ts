import {Entity, ManyToOne, OneToMany} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {PaysEntity} from "./Pays.entity";
import {SiteEntity} from "./Site.entity";

@Entity('Commune')
export class CommuneEntity extends IdAndNameEntity {
    @ManyToOne( () => PaysEntity, pays => pays.communes)
    pays?: PaysEntity
    
    @OneToMany(() => SiteEntity, sites => sites.commune)
    sites?: SiteEntity[]
}