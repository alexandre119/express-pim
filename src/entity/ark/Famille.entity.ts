import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {OrdreEntity} from "./Ordre.entity";
import {GenreEntity} from "./Genre.entity";

@Entity('Famille')
export class FamilleEntity extends IdAndNameEntity{
    @ManyToOne(() => OrdreEntity, ordre => ordre.familles, { nullable: false })
    ordre?: OrdreEntity
    
    @OneToMany(() => GenreEntity, genre => genre.famille)
    genres?: GenreEntity[]
}