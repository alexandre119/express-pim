import {Entity, ManyToOne, OneToMany} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {GenreEntity} from "./Genre.entity";
import {LotEntity} from "./Lot.entity";
import {AlerteEntity} from "./Alerte.entity";

@Entity('Espece')
export class EspeceEntity extends IdAndNameEntity{
    @ManyToOne(() => GenreEntity, genre => genre.especes, { nullable: false })
    genre?: GenreEntity
    
    @OneToMany(() => AlerteEntity, alertes => alertes.espece)
    alertes?: EspeceEntity
    
    @OneToMany( () => LotEntity, lots => lots.espece)
    lots?: LotEntity[]
}