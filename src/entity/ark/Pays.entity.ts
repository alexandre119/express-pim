import {Column, Entity, OneToMany} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {CommuneEntity} from "./Commune.entity";

@Entity("Pays")
export class PaysEntity extends IdAndNameEntity {
    @Column()
    code?: string
    
    @OneToMany(() => CommuneEntity, communes => communes.pays)
    communes?: CommuneEntity[]
}