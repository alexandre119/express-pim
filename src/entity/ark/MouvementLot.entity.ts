import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";

import {LotEntity} from "./Lot.entity";
import {TacheProjetEntity} from "./TacheProjet.entity";
import {SalarieEntity} from "./Salarie.entity";

@Entity('MouvementLot')
export class MouvementLotEntity {
    @PrimaryGeneratedColumn()
    id?: number
    
    @Column()
    date?: Date
    
    @Column()
    destination?: string
    
    @ManyToOne(() => LotEntity, lot => lot.mouvements)
    lot?: LotEntity
    
    @ManyToOne(() => TacheProjetEntity, tache => tache.mouvements)
    tache?: TacheProjetEntity
    
    @ManyToOne(() => SalarieEntity, salarie => salarie.mouvements)
    salarie?: SalarieEntity
}