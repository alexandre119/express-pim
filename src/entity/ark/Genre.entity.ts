import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {FamilleEntity} from "./Famille.entity";
import {EspeceEntity} from "./Espece.entity";

@Entity('Genre')
export class GenreEntity extends IdAndNameEntity{
    @ManyToOne(() => FamilleEntity, famille => famille.genres, { nullable: false })
    famille?: FamilleEntity;
    
    @OneToMany(() => EspeceEntity, espece => espece.genre)
    especes?: EspeceEntity[]
}