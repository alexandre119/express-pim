import {Entity, OneToMany} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {AlerteEntity} from "./Alerte.entity";

@Entity('Sentinelle')
export class SentinelleEntity extends IdAndNameEntity {
    @OneToMany(() => AlerteEntity, alertes => alertes.sentinelle)
    alertes?: AlerteEntity[]
}
