import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {EmbranchementEntity} from "./Embranchement.entity";
import {OrdreEntity} from "./Ordre.entity";

@Entity('Classe')
export class ClasseEntity extends IdAndNameEntity{
    @ManyToOne(() => EmbranchementEntity, embranchement => embranchement.classes, { nullable: false })
    embranchement?: EmbranchementEntity;
    
    @OneToMany(() => OrdreEntity, ordre => ordre.classe)
    ordres?: OrdreEntity[]
}