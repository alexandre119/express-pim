import {Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {EspeceEntity} from "./Espece.entity";
import {MouvementLotEntity} from "./MouvementLot.entity";

@Entity('Lot')
export class LotEntity {
    @PrimaryGeneratedColumn()
    id?: number;
    
    @ManyToOne(() => EspeceEntity, espece => espece.lots, { nullable: false })
    espece?: EspeceEntity
    
    @OneToMany(() => MouvementLotEntity, mouvements => mouvements.lot)
    mouvements?: MouvementLotEntity[]
}