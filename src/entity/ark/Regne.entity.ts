import {Entity, OneToMany} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {EmbranchementEntity} from "./Embranchement.entity";

@Entity('Regne')
export class RegneEntity extends IdAndNameEntity {
    @OneToMany(() => EmbranchementEntity, embranchement => embranchement.regne)
    embranchement?: EmbranchementEntity[]
}