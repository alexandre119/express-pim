import {Entity, ManyToOne, OneToMany} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {CommuneEntity} from "./Commune.entity";
import {SalarieEntity} from "./Salarie.entity";

@Entity('Site')
export class SiteEntity extends IdAndNameEntity {
    @ManyToOne(() => CommuneEntity, commune => commune.sites)
    commune?: CommuneEntity
    
    @OneToMany(() => SalarieEntity, salaries => salaries.site)
    salaries?: SalarieEntity[]
}