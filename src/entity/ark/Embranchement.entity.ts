import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {RegneEntity} from "./Regne.entity";
import {ClasseEntity} from "./Classe.entity";

@Entity('Embranchement')
export class EmbranchementEntity extends IdAndNameEntity{
    @ManyToOne(() => RegneEntity, regne => regne.embranchement, { nullable: false })
    regne?: RegneEntity
    
    @OneToMany(() => ClasseEntity, classe => classe.embranchement)
    classes?: ClasseEntity[]
}