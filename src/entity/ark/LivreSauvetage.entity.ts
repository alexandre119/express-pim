import {Column, Entity, ManyToOne, OneToOne} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {ProjetSauvetageEntity} from "./ProjetSauvetage.entity";

@Entity('LivreSauvetage')
export class LivreSauvetageEntity extends IdAndNameEntity {
    @Column()
    info?: string
    
    @ManyToOne(() => ProjetSauvetageEntity, projetSauvetage => projetSauvetage.livreSauvetage)
    projetSauvetage?: ProjetSauvetageEntity
}