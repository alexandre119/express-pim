import {Column, Entity, ManyToOne, OneToMany} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {SiteEntity} from "./Site.entity";
import {MouvementLotEntity} from "./MouvementLot.entity";

@Entity('Salarie')
export class SalarieEntity extends IdAndNameEntity {
    @Column()
    prenom?: string
    
    @Column()
    adresse?: string
    
    @ManyToOne(() => SiteEntity, site => site.salaries)
    site?: SiteEntity
    
    @OneToMany(() => MouvementLotEntity, mouvements => mouvements.salarie)
    mouvements?: MouvementLotEntity[]
}