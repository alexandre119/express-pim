import {Column, Entity, ManyToOne, OneToMany} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";
import {SentinelleEntity} from "./Sentinelle.entity";
import {ProjetSauvetageEntity} from "./ProjetSauvetage.entity";
import {EspeceEntity} from "./Espece.entity";

@Entity('Alerte')
export class AlerteEntity extends IdAndNameEntity {
    @Column()
    description?: string
    
    @ManyToOne( () => SentinelleEntity, sentinelle => sentinelle.alertes, {nullable: false})
    sentinelle?: SentinelleEntity
    
    @ManyToOne(() => EspeceEntity, espece => espece.alertes, {nullable: false})
    espece?: EspeceEntity
    
    @OneToMany( () => ProjetSauvetageEntity, projetsSauvetage => projetsSauvetage.alerte)
    projetsSauvetage?: ProjetSauvetageEntity[]
}