import {Column, Entity} from "typeorm";
import {IdAndNameEntity} from "./IdAndName.entity";

@Entity('SalleStockage')
export class SalleStockageEntity extends IdAndNameEntity {
    @Column({ nullable: false })
    luminosite?: number
    
    @Column({ nullable: false })
    temperature?: number
    
    @Column({ nullable: false })
    humidite?: number
}