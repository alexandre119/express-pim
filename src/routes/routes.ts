import express from 'express';
import {NextFunction, Request, Response} from 'express';
import {RegneController} from "../controllers/ark/Regne.controller";
import {EmbranchementController} from "../controllers/ark/Embranchement.controller";
import {ClasseController} from "../controllers/ark/Classe.controller";
import {RouteModel, RouteList} from "../models/Route.model";
import {AlerteController} from "../controllers/ark/Alerte.controller";
import {CommuneController} from "../controllers/ark/Commune.controller";
import {EspeceController} from "../controllers/ark/Espece.controller";
import {FamilleController} from "../controllers/ark/Famille.controller";
import {GenreController} from "../controllers/ark/Genre.controller";
import {LivreSauvetageController} from "../controllers/ark/LivreSauvetage.controller";
import {LotController} from "../controllers/ark/Lot.controller";
import {MouvementLotController} from "../controllers/ark/MouvementLot.controller";
import {NarrateurController} from "../controllers/ark/Narrateur.controller";
import {OrdreController} from "../controllers/ark/Ordre.controller";
import {PaysController} from "../controllers/ark/Pays.controller";
import {ProjetSauvetageController} from "../controllers/ark/ProjetSauvetage.controller";
import {SalarieController} from "../controllers/ark/Salarie.controller";
import {SalleStockageController} from "../controllers/ark/SalleStockage.controller";
import {SentinelleController} from "../controllers/ark/Sentinelle.controller";
import {SiteController} from "../controllers/ark/Site.controller";
import {TacheProjetController} from "../controllers/ark/TacheProjet.controller";
import {UserController} from "../controllers/user.controller";

// Liste des routes qui representent les methodes HTTP
export const Routes: Array<RouteModel> = [{
    method: "get",
    route: "",
    action: "all"
}, {
    method: "get",
    route: "/:id",
    action: "one"
}, {
    method: "post",
    route: "",
    action: "save"
}, {
    method: "post",
    route: "/:id",
    action: "update"
}, {
    method: "delete",
    route: "/:id",
    action: "remove"
}];

// Création des routes
export function CreateRoutes() {
    
    // Liste des tables de la bdd
    const controllers: Array<any> = [
        AlerteController,
        ClasseController,
        CommuneController,
        EmbranchementController,
        EspeceController,
        FamilleController,
        GenreController,
        LivreSauvetageController,
        LotController,
        MouvementLotController,
        NarrateurController,
        OrdreController,
        PaysController,
        ProjetSauvetageController,
        RegneController,
        SalarieController,
        SalleStockageController,
        SentinelleController,
        SiteController,
        TacheProjetController,
        UserController
    ];

    // Initialisation des tables et création dans la bdd
    let controllersInstances: Array<any> = [];
    controllers.forEach(controller => {
        controllersInstances.push(new controller())
    });
    
    // Association de chaque table avec son url
    let controllersUrls: Array<any> = [];
    controllersInstances.forEach(controller => {
        controllersUrls.push({
            "url" : controller.url,
            "tableName": controller.tableName
        });
    });
    
    // Route pour retirer la liste des routes de la bdd depuis l'exterieur
    router.get('/getRoutes', ((req, res) => {
        console.log('getRoutes : ', controllersUrls)
        res.send(controllersUrls)
    }));
    
    // Liste des url de tables pour lesquelles il faut créer les routes GET/UPDATE/DELETE
    let RoutesList: Array<RouteList> = [];
    for(let i = 0; i< controllers.length; i++) {
        let controller = controllers[i];
        let url = controllersUrls[i].url;
        Routes.forEach( route => {
            RoutesList.push({
                method: route.method,
                route: '/' + url + route.route,
                controller: controller,
                action: route.action
            });
        });
    }
    console.log('Routes created')
    
    return RoutesList
}

// Parametre du routeur
const router = express.Router({
    caseSensitive: true
});

// Route de test racine
router.get('/', (req: Request, res: Response, next: NextFunction) => {
    res.status(200).json({
        status: 200,
        message: "Router working"
    });
    next();
});

// Route de test api
router.get('/api', (req: Request, res: Response, next: NextFunction) => {
    res.status(200).json({
        status: 200,
        message: "API working"
    });
    next();
});

export default router;